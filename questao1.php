<?php
for ($i = 1; $i < 101; $i ++) {
    echo $i;
    echo multiplo3($i);
    echo multiplo5($i);
    echo "\n<br>";
}

function multiplo3($num)
{
    if ($num % 3 == 0) {
        return " (Fizz)";
    }
}

function multiplo5($num)
{
    if ($num % 5 == 0) {
        return " (FizzBuzz)";
    }
}

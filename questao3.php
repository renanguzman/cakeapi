<?php

(new MyUserClass())->getUserList();

class MyUserClass
{

    public function getUserList()
    {
        $pdo = (new DBConnection())->getConnection();
        $results = $pdo->query('select name from user');
        sort($results);
        
        return $results;
    }
}


class DBConnection
{
    const DBHOST = "localhost";
    const DBUSER = "renan";
    const DBPASSWORD = "";
    const DBNAME = "";
    
    private $hostdb;
    private $userdb;
    private $password;
    private $dbname;
    
    function __construct(
        $host = self::DBHOST, 
        $user = self::DBUSER, 
        $pass = self::DBPASSWORD, 
        $dbname = self::DBNAME){
        
        $this->hostdb = $host;
        $this->userdb = $user;
        $this->password = $pass;
        $this->dbname = $dbname;
        
    }
    
    public function getConnection(){
        try {
            $dbh = new PDO("mysql:host={$this->hostdb};dbname={$this->dbname}", $this->userdb, $this->password);
            return $dbh;
        } catch (PDOException $e) {
            print "Erro de conexao: " . $e->getMessage() . "<br/>\n";
            die();
        }
         
    }
}
<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tarefas Controller
 *
 * @property \App\Model\Table\TarefasTable $Tarefas
 *
 * @method \App\Model\Entity\Tarefa[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TarefasController extends AppController
{

    public function initialize()
    {
        
        parent::initialize();
        $this->loadComponent('RequestHandler');
        
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $tarefas = $this->paginate($this->Tarefas);

        //$this->set(compact('tarefas'));
        $this->set([
            'tarefas'=>$tarefas,
            '_serialize'=>['tarefas']
        ]);
    }

    /**
     * View method
     *
     * @param string|null $id Tarefa id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tarefa = $this->Tarefas->get($id, [
            'contain' => []
        ]);

        $this->set([
            'tarefa' => $tarefa,
            '_serialize'=>['tarefa']
        ]);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tarefa = $this->Tarefas->newEntity();
        $message = "";
        if ($this->request->is('post')) {
            $tarefa = $this->Tarefas->patchEntity($tarefa, $this->request->getData());
            if ($this->Tarefas->save($tarefa)) {
                $message = "The tarefa has been saved.";
                $this->Flash->success(__('The tarefa has been saved.'));

                //return $this->redirect(['action' => 'index']);
            }else{
                $message = "The tarefa could not be saved. Please, try again.";
                $this->Flash->error(__('The tarefa could not be saved. Please, try again.'));
            }
        }
        //$this->set(compact('tarefa'));
        $this->set([
            'message' => $message,
            'tarefa' => $tarefa,
            '_serialize' => ['message', 'tarefa']
        ]);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tarefa id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tarefa = $this->Tarefas->get($id, [
            'contain' => []
        ]);
        $message = "The tarefa could not be saved. Please, try again.";
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tarefa = $this->Tarefas->patchEntity($tarefa, $this->request->getData());
            if ($this->Tarefas->save($tarefa)) {
                $message = "The tarefa has been saved.";
                $this->Flash->success(__('The tarefa has been saved.'));

                //return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tarefa could not be saved. Please, try again.'));
        }
        //$this->set(compact('tarefa'));
        $this->set([
            'message'=>$message,
            'tarefa'=>$tarefa,
            '_serialize'=>['message','tarefa']
        ]);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tarefa id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tarefa = $this->Tarefas->get($id);
        if ($this->Tarefas->delete($tarefa)) {
            $message = "The tarefa has been deleted.";
            $this->Flash->success(__('The tarefa has been deleted.'));
        } else {
            $message = "The tarefa could not be deleted. Please, try again.";
            $this->Flash->error(__('The tarefa could not be deleted. Please, try again.'));
        }
        $this->set([
            'message'=>$message,
            '_serialize'=>['message']
        ]);

        //return $this->redirect(['action' => 'index']);
    }
}

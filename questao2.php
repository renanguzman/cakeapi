<?php
if (userLogged()) {
    header("Location: http://www.google.com");
}

function userLogged()
{
    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
        return true;     
    } elseif (isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin'] == true) {
        return true;
    }else{
        return false;
    }
}
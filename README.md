# README #

## Configuração requerida: ##
* PHP5.6+
* Mysql

### Questão 1 ###
Resposta em: questao1.php


### Questão 2 ###
Resposta em: questao2.php


### Questão 3 ###
Resposta em: questao3.php


### Questão 4 ###
API / Crud Utilizando Cake3

## Diagrama relacional de entidades: ##
cakeapi/documentos/modelo_DER.mwb  (Arquivo mysql workbench)
(Criar base de dados teste e sincronizar tabelas utilizando o modelo acima)


## Configuração de Banco de dados na Aplicação ##
cakeapi/config/app.php
alterar: user / password 
para configurações locais de acesso ao banco de dados Mysql


### Rodar projeto com Cake3 ###

acessar: localdoprojeto/cakeapi/bin
executar comando: cake server


### Acesso ao Crud de Tarefas ###
http://localhost:8765/tarefas

### Funções API ###
* Os serviços da API retornam resultados em JSON e XML, basta trocar a extensão ao final do endereço do serviço solicitado

- Listagem de todos os registros
[GET] - http://localhost:8765/tarefas/index.json

- Listagem de registro especifico
[GET] - http://localhost:8765/tarefas/view/[1].json

- Adicionar registro
[POST] - http://localhost:8765/tarefas
params:
titulo - obrigatorio
descricao - obrigatorio
prioridade

- Editar registro
[PUT] - http://localhost:8765/tarefas/[1]   (numero do id)

- Deletar registro
[DELETE] - http://localhost:8765/tarefas/[1]   (numero do id)
